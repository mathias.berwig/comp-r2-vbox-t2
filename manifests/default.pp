# Define o PATH do sistema
Exec { path => [ "/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin" ] }

# Instala o pacote nginx e vsftpd após garantir que a lista de pacotes está atualizada
package { [ 'nginx', 'vsftpd' ]:
  require => Class["system-update"],
  ensure  => 'installed',
}

# Copia os arquivos de README para raiz /var/www/
exec {"copia_readme":
     command => "sudo cp /vagrant/README.html /usr/share/nginx/www/index.html",
    onlyif => ['ls -l /vagrant/README.html'],
    require => Package["nginx"]
}

# Copia os arquivos de configuração do servidor FTP & Reinicia-o
exec {"copia_config":
    command => "sudo cp -rf /vagrant/vsftpd.conf /etc/vsftpd.conf; sudo restart vsftpd",
    onlyif => ['ls -l /etc/vsftpd.conf'],
    require => Package["vsftpd"]
}

# Certifica-se que os serviços nginx está em execução
service { "nginx":
  ensure  => "running",
  require => Package["nginx"],
}

# Instala o módulo system-update
include system-update
