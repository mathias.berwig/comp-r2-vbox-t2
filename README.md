# Redes II - Trabalho 2 - Virtualização FTP

Este trabalho foi desenvolvido durante a disciplina de Redes II do curso de Ciência da Computação na UNIJUÍ e tem como objetivo demonstrar o procedimento para criar um ambiente virtualizado contendo o servidor web Nginx (reutilizando a implementação do trabalho anterior) e adicionar um servidor FTP.

## Metodologia

Este repositório possui as configurações necessárias para executar um servidor web Nginx e FTP vsftpd.

## Workspace

Para configurar o ambiente de trabalho, é necessário instalar o Vagrant, Puppet e VirtualBox na máquina. É possível fazer isso em um sistema operacional Linux baseado em Debian utilizando o seguinte comando:

`sudo apt install vagrant puppet virtualbox`

## Execução

Após instalar as dependências necessárias, basta navegar até a pasta deste README  iniciar o Vagrant:

`vagrant up`

Quando o comando for concluído, o endereço HTTP [`http:\\192.168.33.16`](http:\\192.168.33.16) estará disponível com um arquivo README; e o endereço FTP [`ftp://192.168.33.16`](ftp://192.168.33.16) estará acessível com as credenciais `vagrant` `vagrant`.